/* Const declaration */
const int sensorInPin = A4;  // Analog input pin that the potentiometer is attached to
const int floorInPin = A1;  // Analog input pin that the potentiometer is attached to
const int ceilingInPin = A0;  // Analog input pin that the potentiometer is attached to

const int ledPin = 13; // digital output pin that the LED is attached to
const int relayPin = 8; // digital output pin that the relay is attached to

const int sampleInterval = 50; // how frequently to sample in ms
const int toggleRelayThreshold = 5000; // ms to wait before allowing relay change (stop relay burnout) 
const unsigned long maxRunningTime = 600000; // longest duration the relay pin may be set to on
const unsigned long minRestTime = 0; // minimum rest time after pin has been on for the duration - set to zero to have no duty cycle
const unsigned long lookbackPeriod = maxRunningTime + minRestTime;
const int serialOutInterval = 500; // how frequently to serial out
const int sensorRecordInterval = 10000; // how frequently to record the sensor value
const unsigned long sensorRecordPeriodSeconds = (long)3600 * 24; // how frequently to record the sensor value

/* variable declaration */
int sensorValue = 0;        // value read from the pressure transducer
int floorValue = 0; // minimum value the sensor can be before turning active
int ceilingValue = 0; // maximum value the sensor can be before turning inactive
bool activeState = false; // set to true when floor/ceil is exceeded, set to false when the reverse threshold is breached
bool dutyCycleExceeded = false; // set to true when the relay pin has been set to on for too long
bool toggleThresholdChoke = false; // set to true when the relay has been toggled too recently
bool relayState = false; // the actual state of the relay pin
int blinkStyle = 0; // modulus to blink the led
int runningTimeInPeriod = 0; // duration of run time in the duty period
int restTimeInPeriod = 0; // duration of rest time in the duty period
bool relayStateHistory[lookbackPeriod / 1000]; // history of relay per 5 seconds 
long sensorStateHistory[sensorRecordPeriodSeconds / sensorRecordInterval]; // history of sensor per 10s over last 24 hours
unsigned long sensorStateLastRecorded = 0;

unsigned long time;
unsigned long lastSerialOutTime;
unsigned long lastToggleStateTime;


void readInputs() {
  // read the analog in value:
  sensorValue = analogRead(sensorInPin);
  // read the analog in value:
  floorValue = analogRead(floorInPin);
  // read the analog in value:
  ceilingValue = analogRead(ceilingInPin);

  // translate into uniform 0-1000 because wiring
  floorValue = 1024 - floorValue;
  ceilingValue = 1024 - ceilingValue;
  sensorValue = 1024 - sensorValue;
}

// globals everywhere!
void activateIfWithinRange() {
  if ((sensorValue < floorValue) && (activeState == false)) {
    activeState = true;
    Serial.print("Setting to ACTIVE\n");
  } else if ((sensorValue >= ceilingValue) && (activeState == true)) {
    Serial.print("Setting to INACTIVE\n");
    activeState = false;
  }
}

void resetDutyCyle() {
  runningTimeInPeriod = 0;
  restTimeInPeriod = 0;
}

void recordDutyCycle() {
  if (relayState) {
    runningTimeInPeriod += sampleInterval;
  } else {
    restTimeInPeriod += sampleInterval;
  }
  // we've had at least minRestTime in the past duty cycle
  if ((minRestTime > 0) && (restTimeInPeriod >= minRestTime)) {
    
    Serial.print("Rested long enough\n");
    resetDutyCyle();
  }
  // if the sum of resting and running is greated than the duty period then reset 
  // note that this might allow for two running periods to overlap
  // so use a maxRunningTime = max half the actual running time (bad algo)
  if ((runningTimeInPeriod + restTimeInPeriod) >= (minRestTime + maxRunningTime)) {
    Serial.print("Finished duty cycle\n");
    resetDutyCyle();
  }
}

void checkDutyCycleThresholdExceeded() {
  if (minRestTime > 0) {
    if (relayState && (runningTimeInPeriod > maxRunningTime)) {
      Serial.print("DUTY CYCLE EXCEEDED\n");
      dutyCycleExceeded = true;
    } else {
      dutyCycleExceeded = false;
    }
  } else {
    dutyCycleExceeded = false;
  }
}

void checkToggleThresholdExceeded() {
  bool toggleThresholdChokePrevious = toggleThresholdChoke;
  // if the relay has been on longer than the max duty cycle
  toggleThresholdChoke = ((time - lastToggleStateTime) < toggleRelayThreshold);
  if (toggleThresholdChokePrevious != toggleThresholdChoke) {
    if (toggleThresholdChoke) {
      Serial.print("Activated TOGGLE CHOKE\n");
    } else {
      Serial.print("Back within TOGGLE CHOKE threshold\n");
    }
  }
}

void setRelayPin(bool newState) {
  if (relayState != newState) {
    relayState = newState;
    if (relayState) {
      Serial.print("Turning relay ON\n");
    } else {
      Serial.print("Turning relay OFF\n");
    }
    lastToggleStateTime = time;
    digitalWrite(relayPin, relayState);
    digitalWrite(9, relayState);
    digitalWrite(10, relayState);
    digitalWrite(11, relayState);
  }
}

void setLedPin() {
  int flashrate;
  if (sensorValue < floorValue) {
    flashrate = 250;
  } else if (sensorValue >- ceilingValue) {
    flashrate = 1000;
  } else if (toggleThresholdChoke || dutyCycleExceeded) {
    flashrate = 100;
  } else {
    flashrate = 500;
  }

  
  if ((millis() % flashrate) < (flashrate / 2)) {
    digitalWrite(ledPin, true);
  } else {
    digitalWrite(ledPin, false);
  }
}

void handleSerialComms() {
  if ((lastSerialOutTime + serialOutInterval) < time) {
    // print the results to the serial monitor:
    Serial.print("time = ");
    Serial.print(time);
    Serial.print("\tfloor = ");
    Serial.print(floorValue);
    Serial.print("\tceiling = ");
    Serial.print(ceilingValue);
    Serial.print("\tsensor = ");
    Serial.print(sensorValue);
    Serial.print("\tmBar = ");
    Serial.print(sensorValue * 1.5);
    Serial.print("\tthrottle = ");
    Serial.print(toggleThresholdChoke);
    Serial.print("\tdutycycle = ");
    Serial.print(dutyCycleExceeded);
    Serial.print("\trelayState = ");
    Serial.print(relayState);
    Serial.print("\tduty = ");
    Serial.print((float)runningTimeInPeriod / (minRestTime + maxRunningTime));

    Serial.print("\n");
    lastSerialOutTime = time;
  }
}

void setup() {
  // initialize serial communications at 9600 bps:
  Serial.begin(9600);
}

void loop() {
  bool newRelayState = relayState;
  // save current time
  time = millis();
  readInputs();
  activateIfWithinRange();
  recordDutyCycle();
  checkDutyCycleThresholdExceeded();
  checkToggleThresholdExceeded();

  // we want to change state but can we?
  if ((activeState != relayState) && (!toggleThresholdChoke)) {
    newRelayState = activeState;
  }

  // hard override for duty cycle
  if (dutyCycleExceeded) {
    newRelayState = false;
    Serial.print("Hard override triggered for DUTY CYCLE \n");
  }

  // hard override for ceiling
  if (sensorValue >= ceilingValue) {
    newRelayState = false;
    Serial.print("Hard override triggered for CEILING PRESSURE \n");
  }
    
  setRelayPin(newRelayState);
  handleSerialComms();
  setLedPin();
  delay(sampleInterval);
}
